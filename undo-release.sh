#!/bin/bash
# :folding=explicit:notabs=true: <- jedit code folding
IFS=" " # to be safe, make sure "file separator" is a known value

function main {
 releaseinfo
 
 if [ -z "$tagexists" ]; then
  echo "Tag '$latesttag' not found. This script can only remove releases added in add-release style"
 else
  echo "REMOVING RELEASE $latesttag"
  removetag 
  removebranches
  removemain
 fi
}

function releaseinfo { # {{{
 # get master branch
 basicbranch=`git branch | grep "*" | sed "s/* //"`
 
 # find latest logged release
 latesttag=`cat .RELEASES/latest`
 # check through "git tag --list" for tag name
 tagexists=`git tag --list | grep "$latesttag" -o`
} # }}}

function removetag { # {{{
 echo "Removing tag $latesttag..."
 git tag -d "$latesttag"
} # }}}

function removebranches { # {{{
 echo "Removing branch commits"
 commitlist=`cat ".RELEASES/${latesttag}_pkgcommits" | tr "\n" "|" | sed "s/|\$//"`
 IFS="|"
 #onbranches=""
 
 # loop through commit names using | as separator
 for v in $commitlist
 do
  # get what branch commit is on
  branchname=`git branch -a --contains "$v" | sed "s/\s\+//"`
  
  echo "Removing $latesttag from $branchname"
  echo " reverting to $v"
  
  git checkout "$branchname" &&
  git reset --hard "$v"
  # LATER: do something sensible if commit is on multiple branches - warn and quit?
  #  for now, try to only put each commit on one branch!
 done
 
 # reset file separator
 IFS=" "
 # return to master branch
 git checkout "$basicbranch"
 
 # remove pkgcommits log
 rm ".RELEASES/${latesttag}_pkgcommits"
} # }}}

function removemain { # {{{
 echo "Removing main commit"
 commitlist=`cat ".RELEASES/${latesttag}_maincommit"`
 # call the single commit the same variable for code consistency
 v="$commitlist"
 
 echo " reverting to $v"
 # files are not removed from the main commit
 # in the assumption you probably just wanted to reorganise them
 git reset "$v"
 
 # remove main commit log
 rm ".RELEASES/${latesttag}_maincommit"
 # attempt to revert 'latest'
 git checkout "$v" ".RELEASES/latest"
} # }}}

main