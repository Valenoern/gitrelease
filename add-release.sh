#!/bin/bash
# :folding=explicit:notabs=true: <- jedit code folding {{{
tagname="$1"
tagexists=`git tag --list | grep "$tagname" -o`
basicbranch=`git branch | grep "*" | sed "s/* //"`
IFS=" " # to be safe, make sure "file separator" is a known value

function main {
 echo "ADD RELEASE $tagname"
 commit
 createtag
 pkgsort
 #format
 writelogs
} # }}}

# create commit to represent release
function commit { # {{{
 echo "Creating commit"
 
 # start commit message with what release name / tag this is
 msghead="tree $tagname"
 # if there is a README_* file append it to the message
 if [ -f README_${tagname}* ]; then
  readme=$(echo -e "\n\nrelease notes:\n"; cat README_${tagname}*)
 fi
 releasemessage="$msghead $readme"
 
 # open the release message in commit message editor,
 # then commit
 basicbrhead=`git rev-parse refs/heads/$basicbranch`
 git add . && git commit -m "$releasemessage" -e &&
 
 # log operation
 echo "$basicbrhead" > ".git/temp-${tagname}_maincommit"
} # }}}

# create tag if it doesn't exist
function createtag { # {{{
 printf "Creating tag $tagname..."
 if [ -z "$tagexists" ]; then
  # create tag
  git tag $tagname
  # log operation
  echo "$tagname" > ".git/temp-latest"
  printf " done\n"
 else
  printf " already exists\n"
 fi
} # }}}

# send packages to appropriate branches
function pkgsort { # {{{
 # {{{
 
 # if .RELEASES/branches not found, exit this function {{{
 if [ ! -f ".RELEASES/branches" ]; then
  echo ".RELEASES/branches not found, not sorting packages into branches"
 # otherwise keep going
 else
 echo "Sorting packages" # }}}
 
 # initialise variables {
 # separate files by | in upcoming loop
 # (a lot less painful than changing to spaces frankly)
 IFS="|"
 # get all files added by commit to loop over
 # --format is usually used to print commit metadata; leaving it blank omits this
 # then remove last newline / "bogus file" with sed
 addedfiles=`git show --name-only HEAD --format="" | tr "\n" "|" | sed "s/|\$//"`
 
 # get stuff that will be used by pkgsortplace
 # patterns to test each package for
 testpatterns=`cat .RELEASES/branches | tr "\n" "|" | sed "s/|\$//"`
 # get name of current master (etc) commit in order to 'copy' pkgs to branches
 basicbrhead=`git rev-parse refs/heads/$basicbranch`
 testresult="" # }
 
 # reset temporary commit list
 printf "" > ".git/temp-${tagname}_pkgcommits"
 
 # loop through last master commit's files using | as separator
 for v in $addedfiles
 do
  # "git show" can show files that were deleted, so act on file only if it exists
  if [ -f "$v" ]; then
    printf "Placing $v..."
    hadmatches=""
    
    # test package with each branch expression
    for j in $testpatterns
    do
      # do test if package matched branch
      testresult=`pkgsorttest $v $j`
      # if result is non-empty assume matches.
      if [ ! -z "$testresult" ]; then
       echo " => $testresult"
       hadmatches="yes"
       
       # log commit name so it's easier to undo
       # first get package name to log
       pkgbrhead=`git rev-parse refs/heads/$testresult`
       # then log commit's name
       echo "$pkgbrhead" >> ".git/temp-${tagname}_pkgcommits"
       
       #  send package to its branch
       pkgsortplace "$v" "$testresult"
      fi
    done
    
    if [ -z "$hadmatches" ]; then
     printf " didn't match any branches\n"
    fi
  fi
 done
 
 # to be safe, set file separator back to known value
 IFS=" "
 
 fi # end .RELEASES/branches check
} # }}}

# return what branch each package should go to
# these must be specified in .RELEASES/branches as "expr1 expr2"
function pkgsorttest { # {{{
 packagename=$1
 testexpr=`echo "$2" | sed "s/\(.\+\)\s\+\(.\+\)/\1/"`
 branchname=`echo "$2" | sed "s/\(.\+\)\s\+\(.\+\)/\2/"`
 pkgsortstate=`echo "$packagename" | grep "$testexpr" -o`
 
 #printf "\nTEST $packagename - $testexpr -> $pkgsortstate \n"
 
 if [ ! -z "$pkgsortstate" ]; then
  printf "$branchname"
 fi
} # }}}

# check package into its respective branch
function pkgsortplace { # {{{
 packagename=$1
 branchname=$2
 
 printf " adding to $branchname... \n"
 
 # "copy" file from master to here, though in git its data exists just once
 # syntax: git checkout <commit_hash> <relative_path_to_file_or_dir>
 git checkout "$branchname"
 git checkout "$basicbrhead" "$packagename"
 # commit changes to branch
 git add . && git commit -m "\"${branchname}\" packages @ $tagname"
 
 # return to master branch
 git checkout "$basicbranch"
} # }}}
# }}}

# commit logs to master branch
function writelogs { # {{{
 # see if there are any temp files found in .git
 git checkout "$basicbranch"
 cd .git
 logfiles=`find ./temp* -printf "%p|"`
 
 # $? contains the last command's exit status
 # if any files were found it should return 0, otherwise 1
 if [[ "$?" == "0" ]]; then
  echo "Committing log files"
 
  # separate filenames by |
  IFS="|"
  logfiles=`echo "$logfiles" | sed "s/|\$//"`
  
  # loop through temp files in .git dir
  for v in $logfiles
  do
   # remove temp prefix
   nontempname=`echo "$v" | sed "s/\(.\/temp-\)\?\(.\+\)/\2/"`
   cp "$v" "../.RELEASES/$nontempname" &&
   rm "$v"
  done
  
  cd ..
  git add . && git commit --amend --no-edit --quiet
  
  else
  echo "No temporary log files found to clean up"
 fi
} # }}}

# TODO:
 # - test out a good way to version control branch release scripts
 # - auto format debian repos etc

# regenerate special repository format if required
function format { # {{{
 echo "regenerate repository"
 # generate structure
 # commit changes to branch; dont amend for safety
} # }}}

main
