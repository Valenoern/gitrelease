# "documentation"

This is a very experimental tool to create a git-based version of the 'Releases' tab on github or a gitea host, which may eventually also act as a 'universal package warehouse' to somehow generate or put up packages repositories for any package manager.

At this time it's not really ready for wide use and you probably shouldn't use it yet. But, the source is here for you to see how the system works.


## Process to create releases

`add-release.sh <RELEASENAME>` is used to add a release.

* The name for a release is passed in like `add-release.sh 2020-03-08`
* A commit is created
  * the top of the commit message will be labeled with the release name - e.g. "tree 2020-03-08"
  * If there is a `README_*` with the same name as the release, e.g. "README_2020-03-08.md", it will be copied into the commit message as 'release notes'.
  * A git version tag is created to point to the release - e.g. "2020-03-08"
  * when the commit is finished its hash is logged to make it easier to undo

`add-release.sh` assumes that whatever branch name is checked out when it runs is a 'main' branch containing packages for all distros or operating systems.

* Packages will be sorted into branches for each 'distro' if defined
  * A release commit is read for what files it contains
  * if a file on the main branch matches the expression for this distro, it will be 'checked in' from the main branch, using git's internal filesystem features to reference the same data
  * the files(s) are committed to the distro branch based on the branch name + release name: "debian packages @ 2020-03-08"
  * After sorting packages the hash of every extra commit is logged to make them easier to undo

## Creating distro branches

`new-branch.sh <RELEASENAME>` is used to add a distro branch.

* If the branch name doesn't already exist, a new one will be created off the specified "template commit", such as the initial commit.
* Packages are checked in from each commit on the main branch, whether it has an associated tag or not
  * these are currently not committed release-by-release but all to a single commit. That should probably be fixed at some point.

## Internal records

* `.RELEASES` is used to hold information about branches and commits
* `.RELEASES/template` holds the template commit used to create new branches.
* `.RELEASES/branches` must be present to sort packages:
  * each expression should be formatted as `<test-expr> <branchname>`
    * e.g. `.tar.gz test`, `_debian.tar.gz debian`, `_windows.tar.gz windows`
    * these expressions will be tested as literal text, as in any file ending with `.tar.gz` that doesn't end with the other two expressions will be sent to branch `test`.
* `.RELEASES/latest` and `.RELEASES/<RELEASENAME>_maincommit` log the tag name and hash **before** a release commit respectively
  * `.RELEASES/<RELEASENAME>_pkgcommits` log all the hashes **before** commits splintering this release to distro branches

## Undoing releases

`undo-release.sh` can remove commits from the latest release.

* `.RELEASES/latest` is read for the latest release tag name, e.g. "2020-03-08"
* The git tag with this name is removed
* `.RELEASES/<RELEASENAME>_pkgcommits` is read to remove distro commits
  * each of these commits is the **parent** of the commit that was laid down, so it is used to `git reset --hard` to the previous commit
  * for now the script assumes _each distro commit will only be on one branch_.
* `.RELEASES/<RELEASENAME>_maincommit` is read to remove the main commit
  * like the distro commits, this is the **parent** of the main commit, so it is used to `git reset` to the previous commit
  * the main branch is not hard reset so that you can redo the `add-release` process and re-commit the files after correcting whatever error you may have made.


## TODO

* Many things about this process may be lacking, but I don't know what the problems are at this time
* These scripts were initially written just a split second before the "renaming master to main" discussion. references to "master" will probably be gradually removed as all my test repositories move to main
* some way to never lose release tags when mirroring a repo?


## License

This program is released under the GPL3.
