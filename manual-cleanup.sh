#!/bin/bash

# script for me to manually clean up an add release test,
# if there are bugs in undo-release, etc.
# there is probably no reason for you to run this.

git checkout master &&

git tag -d "bogus-06-08";
rm .RELEASES/bogus*;
rm .git/temp-*;

git reset --hard f708c104c442e68e5f23331c7339a7ee34d087e0 &&
git checkout windows &&
git reset --hard 4684d6c22495a6c6737d61c77139dcb3e5902798 &&
git checkout debian &&
git reset --hard 73c3ecbf4d0fb4b3c289bbb80165973c8ecc6eda
git checkout master


# notes
# - cleaning tree: rm *.tar.gz README_*
# - you can set date on commits with git commit --date
