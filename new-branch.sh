#!/bin/bash
# :folding=explicit:notabs=true: <- jedit code folding
IFS=" " # to be safe, make sure "file separator" is a known value
branchname="$1"
basicbranch=`git branch | grep "*" | sed "s/* //"`
basicbrhead=`git rev-parse refs/heads/$basicbranch`
latesttag=`cat .RELEASES/latest`
template=`cat .RELEASES/template`

function main {
 createbranch
 populate
}

function createbranch { # {{{
 printf "Creating branch $branchname..."
 
 branchexists=`git branch | grep "$branchname" | sed "s/\s\+//"`
 
 # if branch doesn't exist,
 # create it off the commit in .RELEASES/template
 if [ -z "$branchexists" ]; then
  git branch "$branchname" "$template"
  printf " done\n"
 else
  printf " already exists\n"
 fi
 
 # these branches can be deleted with git branch -d branchname
 # just like any other branch can
} # }}}

# populate branch with all previous packages found on master
function populate {	# {{{
 # first make sure there's a branch patterns file
 if [ ! -f ".RELEASES/branches" ]; then
  echo ".RELEASES/branches not found, not populating packages"
 else
  IFS="|"
  
  # second, check the patterns file for if this branch has a pattern
  patternexists=`grep "$branchname" ".RELEASES/branches"`
  if [ -n "$patternexists" ]; then
    # if a pattern exists, prepare
    pattern=`echo "$patternexists" | sed "s/\(.\+\)\s\+\(.\+\)/\1/"`
    
    echo "Populating branch with files matching: $pattern"
    
    # pick up list of commits to pull from
    commitlist=`git log --format="%H" | tr "\n" "|" | sed "s/|\$//"`
    git checkout "$branchname"
    
    # loop through files in one commit
    for v in $commitlist
    do
      #echo "COMMIT: $v"
      treefiles=`git ls-tree "$v" --name-only | tr "\n" "|" | sed "s/|\$//"`
      # LATER: it's possible to limit the # of commits with --max-count=<number>
      
      for j in $treefiles
      do
        # if file in commit matches this branch's pattern
        testresult=`pkgsorttest "$j" "$pattern"`
        if [ -n "$testresult" ]; then
          echo " $j"
          git checkout "$v" "$j"
        fi
      done
    done
    # end looping files
  fi # end walking log
  
  # if no latest release, use the most recent commit as reference
  headname=$latesttag
  tagexists=`git tag --list | grep "$latesttag" -o`
  if [ -z "$tagexists" ]; then
   headname="$basicbranch $basicbrhead"
  fi
  
  # commit changes to branch
  echo "Committing packages to branch"
  git add . && git commit -m "\"${branchname}\" packages @ $headname"
 fi
} # }}}

# COPIED FROM ADD-RELEASE FOR NOW:
# return what branch each package should go to
# these must be specified in .RELEASES/branches as "expr1 expr2"
function pkgsorttest { # {{{
 packagename=$1
 testexpr=`echo "$2" | sed "s/\(.\+\)\s\+\(.\+\)/\1/"`
 branchname=`echo "$2" | sed "s/\(.\+\)\s\+\(.\+\)/\2/"`
 pkgsortstate=`echo "$packagename" | grep "$testexpr" -o`
 
 #printf "\nTEST $packagename - $testexpr -> $pkgsortstate \n"
 
 if [ ! -z "$pkgsortstate" ]; then
  printf "$branchname"
 fi
} # }}}

main